# Course Slider #

A simple course slider block that displays a chosen list of courses in a carousel.

The course slider block uses the popular slick jquery slider.  Various options are available to configure for each 
block, as well as general settings, including:

- Styling, such as border, image height
- Navigation: Number of slides, navigation arrows and more
- Custom CSS and Javascript file inclusion

  * Version 1.0 (2017053000)

### How do I get set up? ###

Clone this repository directly under <Moodle installation directory>/blocks/course_slider or 
download the zip file to <Moodle installation directory>/blocks/ and unzip it there.

## Settings ##

Site-wide configuration options are available under: 
Site Administration -> Plugins -> Blocks -> Course slider

Block settings are available by editing block configuration.


### Contribution ###

Developed by:

 * Kyriaki Hadjicosta (Coventry University)
 * Manoj Solanki (Coventry University)

Co-maintained by:

 * Jeremy Hopkins (Coventry University)
 * Fernando Acedo (3-bits.com)


### Compatibility ###

- Moodle 3.2, 3.3
- Adaptable version 1.4


### Licenses ###

Adaptable is licensed under:
GPL v3 (GNU General Public License) - http://www.gnu.org/licenses

The Font Awesome font (by Dave Gandy) http://fontawesome.io) is licensed under:
SIL Open Font License v1.1 - http://scripts.sil.org/OFL 

Font Awesome CSS, LESS, and SASS files are licensed under:
MIT License - https://opensource.org/licenses/mit-license.html
